import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {Store} from '@ngrx/store';

import * as AuthActions from './store/auth.actions';
import * as fromApp from '../store/app.reducer';


@Injectable({
  providedIn: 'root'
})

export class AuthService {
  token: string = null;
  tokenExpirationTimer: any;

  constructor(
    private router: Router,
    private store: Store<fromApp.AppState>
  ) {
  }

  setLogoutTimer(expirationDuration: number) {
    this.tokenExpirationTimer = setTimeout(() => {
      this.store.dispatch(new AuthActions.Logout());
    }, expirationDuration);
  }

  clearLogoutTimer() {
    if (this.tokenExpirationTimer) {
      clearInterval(this.tokenExpirationTimer);
      this.tokenExpirationTimer = null;
    }
  }
}
