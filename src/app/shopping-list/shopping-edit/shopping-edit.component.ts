import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Store} from '@ngrx/store';

import * as fromApp from '../../store/app.reducer';
import * as ShoppingListActions from '../store/shopping-list.actions';
import {Subscription} from 'rxjs';
import {Ingredient} from '../../shared/ingredient.model';

@Component({
  selector: 'app-shopping-edit',
  templateUrl: './shopping-edit.component.html',
  styleUrls: ['./shopping-edit.component.scss']
})
export class ShoppingEditComponent implements OnInit, OnDestroy {
  addIngredientForm: FormGroup;

  subscription: Subscription;

  editMode = false;
  editedItem: Ingredient;

  constructor(
    private store: Store<fromApp.AppState>
  ) {
  }

  ngOnInit() {
    this.addIngredientForm = new FormGroup({
      name: new FormControl('', {
        updateOn: 'change',
        validators: [Validators.required]
      }),
      amount: new FormControl('', {
        updateOn: 'change',
        validators: [Validators.required, Validators.min(0)]
      })
    });

    this.subscription = this.store.select('shoppingList')
      .subscribe(stateData => {
        if (stateData.editedIngredientIndex > -1) {
          this.editMode = true;
          this.editedItem = stateData.editedIngredient;
          this.addIngredientForm
            .setValue({name: this.editedItem.name, amount: this.editedItem.amount});
        } else {
          this.editMode = false;
        }
      });
  }

  onSubmit() {
    const ingredient = new Ingredient(
      this.addIngredientForm.value.name,
      +this.addIngredientForm.value.amount
    );
    if (!this.editMode) {
      this.store.dispatch(new ShoppingListActions.AddIngredient(ingredient));
    } else {
      this.store.dispatch(new ShoppingListActions.UpdateIngredient(ingredient));
    }
    this.onClear();
  }

  onClear() {
    this.addIngredientForm.reset();
    this.editMode = false;
    this.store.dispatch(new ShoppingListActions.StopEdit());
  }


  onDelete() {
    if (this.editMode) {
      this.store.dispatch(
        new ShoppingListActions.DeleteIngredients()
      );
      this.onClear();
    }
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    this.store.dispatch(new ShoppingListActions.StopEdit());
  }
}
