import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormArray, FormControl, FormGroup, Validators} from '@angular/forms';
import {Store} from '@ngrx/store';

import {map} from 'rxjs/operators';
import * as fromApp from '../../store/app.reducer';
import * as RecipesActions from '../store/recipe.actions';
import {Recipe} from '../recipe.model';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-recipe-edit',
  templateUrl: './recipe-edit.component.html',
  styleUrls: ['./recipe-edit.component.scss']
})
export class RecipeEditComponent implements OnInit, OnDestroy {
  id: number;
  editMode = false;
  recipeForm: FormGroup;
  private storeSub: Subscription;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private store: Store<fromApp.AppState>
  ) {
  }

  ngOnInit() {
    this.route.paramMap.subscribe(paramMap => {
      if (!paramMap.has('id')) {
        this.initForm();
        return;
      }
      this.id = +paramMap.get('id');
      this.editMode = true;
      this.initForm();
    });
  }

  private initForm() {
    let recipeName = '';
    let recipeImagePath = '';
    let description = '';
    const recipeIngredients = new FormArray([]);
    if (this.editMode) {
      this.storeSub = this.store.select('recipes')
        .pipe(
          map(recipeState => {
            return recipeState.recipes.filter(recipes => {
              return recipes.id === this.id;
            });
          })
        )
        .subscribe(recipeArr => {
          const recipe = recipeArr[0];
          recipeName = recipe.name;
          recipeImagePath = recipe.imagePath;
          description = recipe.description;
          if (recipe.ingredients) {
            for (let ingredient of recipe.ingredients) {
              recipeIngredients.push(
                new FormGroup({
                  name: new FormControl(ingredient.name, {
                    updateOn: 'change',
                    validators: [Validators.required]
                  }),
                  amount: new FormControl(ingredient.amount, {
                    updateOn: 'change',
                    validators: [
                      Validators.required,
                      Validators.pattern(/^[1-9]+[0-9]*$/)
                    ]
                  })
                })
              );
            }
          }
        });
    }

    this.recipeForm = new FormGroup({
      name: new FormControl(recipeName, {
        updateOn: 'change',
        validators: [Validators.required]
      }),
      imagePath: new FormControl(recipeImagePath, {
        updateOn: 'change',
        validators: [Validators.required]
      }),
      description: new FormControl(description, {
        updateOn: 'change',
        validators: [Validators.required]
      }),
      ingredients: recipeIngredients
    });
  }

  get controls() {
    return (this.recipeForm.get('ingredients') as FormArray).controls;
  }

  onSubmit() {
    let id;
    if (this.editMode) {
      id = this.id;
    } else {
      id = Math.floor(Math.random() * 10000);
    }
    const newRecipe = new Recipe(
      this.recipeForm.value.name,
      this.recipeForm.value.description,
      this.recipeForm.value.imagePath,
      this.recipeForm.value.ingredients,
      id
    );
    if (this.editMode) {
      this.store.dispatch(new RecipesActions.UpdateRecipe({index: this.id, newRecipe}));
    } else {
      this.store.dispatch(new RecipesActions.AddRecipe(newRecipe));
    }
    this.onCancel();
  }

  onAddIngredient() {
    (<FormArray> this.recipeForm.get('ingredients')).push(
      new FormGroup({
        name: new FormControl(null, {
          updateOn: 'change',
          validators: [Validators.required]
        }),
        amount: new FormControl(null, {
          updateOn: 'change',
          validators: [
            Validators.required,
            Validators.pattern(/^[1-9]+[0-9]*$/)
          ]
        })
      })
    );
  }

  onCancel() {
    this.router.navigate(['../'], {relativeTo: this.route});
  }

  onDeleteIngredient(index: number) {
    (<FormArray> this.recipeForm.get('ingredients')).removeAt(index);
  }

  ngOnDestroy(): void {
    if (this.storeSub) {
      this.storeSub.unsubscribe();
    }
  }
}
