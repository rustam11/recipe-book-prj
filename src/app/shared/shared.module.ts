import {NgModule} from '@angular/core';
import {AlertComponent} from './alert/alert.component';
import {CommonModule} from '@angular/common';
import {DropdownDirective} from './dropdown.directive';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [AlertComponent, DropdownDirective],
  imports: [
    CommonModule,
    NgbModule
  ],
  exports: [
    AlertComponent,
    CommonModule,
    NgbModule,
    DropdownDirective
  ],
  entryComponents: [AlertComponent]
})

export class SharedModule {

}
